var express = require('express');
var app = express();
var path = require('path');
app.use('/static', express.static(path.join('/home/pi/projects' + '/public')));
var Promise = require('es6-promise').Promise;
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.get('/health', function (req, res) {
  res.send('{"status":"ok"}');
});

var gpio = require('rpi-gpio');
gpio.setup(7, gpio.DIR_IN, readInput);
function readInput() {
   gpio.read(7, function handleGPIORead(err, value) {
        console.log('The GPIO Pin Value is ' + value);
   });

}

app.get('/api/gpio/pins/:pinId', function (req, res) {
  var pinId = req.params.pinId;	
  var value = "";
  var p1 = new Promise(function(resolve, reject) {
       			gpio.read(7, function handleGPIORead(err, value) {
 console.log('GET /api/gipio/pins/:pinId | Pin 7 | value=' + value);                                
 resolve(value);
   				});
     		});

  p1.then(function(val) {
 	 console.log('Fulfillment value from promise is ' + val);
 	 res.send('{"pinId": '+pinId+', "value":'+val+'}');
	}, function(err) {
	  console.log('Result from promise is', err);
	 res.send('{"pinId": '+pinId+', "value":"Error"}');
	});

});


 


app.post('/api/readings', function(req, res) {
    var d = new Date();
    var value = req.body.value;
    var type = req.body.type;

    res.send('{ "value":'+value + ', "date": \"' + d.toLocaleDateString()+'\" , "time": \"'+ d.toLocaleTimeString() + '\" , "type": \"' + type+'\" }');
    console.log('Reading | '+d.toLocaleDateString()+'|'+d.toLocaleTimeString()+' | value='+value+' | type='+type);
    var readingsJSON =new File('/home/pi/projects/public/readings.json');
    txtFile.writeln(output);
    txtFile.close();

});

app.listen(8080, function () {
  console.log('API Running ...');
});
