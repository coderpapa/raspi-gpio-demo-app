# README #


### What is this repository for? ###

* Node.js application with some express, rpi-gpio and async api to read/update GPIO pins. 
* V0.1
* See Blog: https://techiecook.wordpress.com/2016/10/11/raspberry-pi-iot-api-setup-part-ii/


### How do I get set up? ###

* Grab a Raspberry Pi

* Install Node 
```
#!bash

wget https://nodejs.org/download/release/v0.10.0/node-v0.10.0-linux-arm-pi.tar.gz
cd /usr/local
sudo tar xzvf ~/node-v0.10.0-linux-arm-pi.tar.gz --strip=1
node -v 
npm -v 
```


* Install Express

```
#!bash
npm install express --save
```

* Install rpi-gpio

```
#!bash
npm install rpi-gpio
```

* Install Promises Library

```
#!bash
npm install es6-promise@3.1.2
```

* Clone this repo and cd into the source and run  `sudo node app.js`